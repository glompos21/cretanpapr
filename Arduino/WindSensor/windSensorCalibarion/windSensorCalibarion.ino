
/* Modern Device Wind Sensor Sketch for Rev C Wind Sensor
  This sketch is only valid if the wind sensor if powered from
  a regulated 5 volt supply. An Arduino or Modern Device BBB, RBBB
  powered from an external power supply should work fine. Powering from
  USB will also work but will be slightly less accurate in our experience.

  When using an Arduino to power the sensor, an external power supply is better. Most Arduinos have a
  polyfuse which protects the USB line. This fuse has enough resistance to reduce the voltage
  available to around 4.7 to 4.85 volts, depending on the current draw.

  The sketch uses the on-chip temperature sensing thermistor to compensate the sensor
  for changes in ambient temperature. Because the thermistor is just configured as a
  voltage divider, the voltage will change with supply voltage. This is why the
  sketch depends upon a regulated five volt supply.

  Other calibrations could be developed for different sensor supply voltages, but would require
  gathering data for those alternate voltages, or compensating the ratio.

  Hardware Setup:
  Wind Sensor Signals    Arduino
  GND                    GND
  +V                     5V
  RV                     A1    // modify the definitions below to use other pins
  TMP                    A0    // modify the definitions below to use other pins

  Paul Badger 2014

  Hardware setup:
  Wind Sensor is powered from a regulated five volt source.
  RV pin and TMP pin are connected to analog innputs.

*/


#define analogPinForRV    1   // change to pins you the analog pins are using
#define analogPinForTMP   0

// to calibrate your sensor, put a glass over it, but the sensor should not be
// touching the desktop surface however.
// adjust the zeroWindAdjustment until your sensor reads about zero with the glass over it.

const float zeroWindAdjustment =  .2; // negative numbers yield smaller wind speed0_100s and vice versa.

int TMP_Therm_ADunits;  //temp termistor value from wind sensor
float RV_Wind_ADunits;    //RV output from wind sensor
float RV_Wind_Volts;
unsigned long lastMillis = 0;
int TempCtimes100;
float zeroWind_ADunits;
float zeroWind_volts;
float Windspeed0_100_MPH;
float Windspeed0_100_KMH;

//motor control
int speed0_100 = 0;
const int motor = 3;


// how much serial data we expect before a newline
const unsigned int MAX_INPUT = 1;



void setup() {

  Serial.begin(57600);   // faster printing to get a bit better throughput on extended info
  // remember to change your serial monitor

  Serial.println("start");

}


// here to process incoming serial data after a terminator received
void process_data (const char * data)
{
  // for now just display it
  // (but you could compare it to some value, convert to an integer, etc.)
  Serial.println (data);
}  // end of process_data

void processIncomingByte (const byte inByte)
{
  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
  {

    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data (input_line);

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = inByte;
      break;

  }  // end of switch

} // end of processIncomingByte

void calculatespeed0_100() {

  TMP_Therm_ADunits = analogRead(analogPinForTMP);
  RV_Wind_ADunits = analogRead(analogPinForRV);
  RV_Wind_Volts = (RV_Wind_ADunits *  0.0048828125);

  // these are all derived from regressions from raw data as such they depend on a lot of experimental factors
  // such as accuracy of temp sensors, and voltage at the actual wind sensor, (wire losses) which were unaccouted for.
  TempCtimes100 = (0.005 * ((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits)) - (16.862 * (float)TMP_Therm_ADunits) + 9075.4;

  zeroWind_ADunits = -0.0006 * ((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits) + 1.0727 * (float)TMP_Therm_ADunits + 47.172; //  13.0C  553  482.39

  zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - zeroWindAdjustment;

  // This from a regression from data in the form of
  // Vraw = V0 + b * Windspeed0_100 ^ c
  // V0 is zero wind at a particular temperature
  // The constants b and c were determined by some Excel wrangling with the solver.

  Windspeed0_100_MPH =  pow(((RV_Wind_Volts - zeroWind_volts) / .2300) , 2.7265);
  Windspeed0_100_KMH = 1.609344 * Windspeed0_100_MPH;
  Serial.print("Windspeed0_100 Km/h ");
  Serial.print((float)Windspeed0_100_KMH);
}

unsigned int integerValue = 0; // Max value is 65535
char incomingByte;
void loop() {

  //if (Serial.available() > 0) {   // something came across serial
  integerValue = 0;         // throw away previous integerValue
  while (1) {           // force into a loop until 'n' is received
    if (millis() - lastMillis > 200) {
      calculatespeed0_100();
      Serial.print("  speed0_100 = ");
      Serial.println(speed0_100);
      lastMillis = millis();
    }
    incomingByte = Serial.read();
    if (incomingByte == '\n') break;   // exit the while(1), we're done receiving
    if (incomingByte == -1) continue;  // if no characters are in the buffer read() returns -1
    integerValue *= 10;  // shift left 1 decimal place
    // convert ASCII to integer, add, and shift left 1 decimal place
    integerValue = ((incomingByte - 48) + integerValue);
    speed0_100 = integerValue;
  }
  Serial.println(integerValue);   // Do something with the value
  speed0_100 = constrain(speed0_100, 0, 100);
  byte value = map(speed0_100, 0, 100, 0, 255);
  analogWrite(motor, value);


}



